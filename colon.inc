%define next 0
%macro colon 2
	%ifid %2
		%2: dq next
		%define next %2
	%else
		%error "Second Arg must be id"
	%endif
	
	%ifstr %1
		db %1, 0
	%else
		%error "Firs arg mus be string"	
	%endif
%endmacro
