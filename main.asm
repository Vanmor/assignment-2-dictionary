%include "words.inc"
%include "lib.inc"
section .data
noSuchElement: db "The list doesn,t contain this element", 0
section .bss
buffer: times 256 db 0
section .rodata
buffOverflow: db "Message must be less than 255 symblos"

global _start
extern find_word

section .text

_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	cmp rax, 0
	je .BufferOverflow
	mov rdi, buffer
	mov rsi, next
	push rsi
	call find_word
	pop rsi
	cmp rax, 0
	je .elementNotFound
	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	call exit	

.BufferOverflow:
	mov rdi, buffOverflow
	call .printError

.elementNotFound:
	mov rdi, noSuchElement
	call .printError

;.printError:
	;push rdi
	;call string_length
	;mov rdx, rax
	;pop rdi
	;mov rsi, rdi
	;mov rax, 1
	;mov rdi, 2
	;syscall
	;call print_newline
	;call exit
	
	
	
