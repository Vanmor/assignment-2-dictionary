global find_word
extern string_equals
section .text

find_word:
	.loop:
		;cmp rsi, 0
		;je .not_found

		push rsi
		push rdi
		add rsi, 8
		call string_equals
		pop rdi
		pop rsi
		
		cmp rax, 1
		je .found
		
		mov rsi, [rsi]
		cmp rsi, 0
		jne .loop
	xor rax,rax
	jmp .not_found

	.not_found: 
		mov rax, 0 
		ret
	.found:
		mov rax, rsi
		ret
