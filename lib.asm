section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global string_equals

 
; Принимает код возврата и завершает текущий процесс
exit: 
    	;xor rax, rax,
	mov rax, 60
	;xor rdi, rdi
	syscall
    	 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.loop:
	cmp byte [rdi+rax], 0
	jz .end
	inc rax
	jnz .loop
	.end:
   	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    	xor rax, rax
		mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    	xor rax, rax
	push rax
	push rcx
	mov rdx, 1
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	syscall
	pop rdi
	pop rcx
	pop rax
    	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
    	call print_char
    	


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    	xor rax, rax
	xor rcx, rcx
	mov rax, rdi
	mov rdi, 0xA
	.getDigit:
	xor rdx, rdx
	div rdi
	inc rcx
	push rdx
	cmp rax, 0
	je .out
	jmp .getDigit
	.out:
	dec rcx
	pop rdi
	add rdi, 0x30
	call print_char
	cmp rcx, 0
	je .end
	jmp .out
	.end:
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    	xor rax, rax
	cmp rdi, 0
	jl .negative
	call print_uint
	ret
	.negative:
	push rdi
	mov rdi, 45
	call print_char
	pop rdi
	neg rdi
	call print_uint
	xor rax, rax
   	 ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    	xor rax, rax
	call string_length
	mov rcx, rax
	push rdi
	mov rdi, rsi
	call string_length
	pop rdi
	cmp rax, rcx
	push r13
	jne .notEquals
	xor r13, r13
	.eqLoop:
	mov al, byte[rsi+r13]
	cmp byte[rdi+r13], al
	jne .notEquals
	inc r13
	cmp rcx, r13
	jle .end
	jmp .eqLoop
	.end:
	mov rax, 0x1
	pop r13
	ret
	.notEquals:
	mov rax, 0x0
	pop r13
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    	xor rax, rax
	mov rdx, 1
	mov rdi, 0 ; deskriptor - STDIN
	push rdi
	mov rsi, rsp
	syscall
	pop rax
    	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rax, rax
	xor rcx, rcx
	mov r9, rdi
	mov r8, rsi
	push rdi
	.loopSpace:
	call read_char
	cmp rax, 0
	je .nullStr
	cmp rax, 0x20
	jne .notSpace
	je .cont
	.notSpace:
	cmp rax, 0x9
	jne .notTab
	je .cont
	.notTab:
	cmp rax, 0xA
	jne .normalWord
	.cont:
	inc r9
	jmp .loopSpace

	.normalWord:
	xor rcx, rcx
	inc rcx
	mov [r9], rax
	inc r9
	.loop:
	push rcx
	call read_char
	pop rcx
	mov [r9], rax
	cmp rax, 0
	je .end
	cmp rax, 0x20
	je .end
	cmp rax, 0x9
	je .end
	cmp rax, 0xA
	je .end
	inc rcx
	inc r9
	cmp rcx, r8
	je .endBuffer
	jmp .loop
	.end:
	inc r9
	mov byte[r9], 0
	mov rdx, rcx
	pop rax
	ret
	.nullStr:
	xor rdx, rdx
	pop rax
	ret
	.endBuffer:
	pop rax
	xor rax, rax
    	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    	xor rax, rax
	xor rdx, rdx 
	xor rcx, rcx

	.loop:
	xor rcx, rcx
	mov cl, [rdi+rdx] 
	cmp cl, 0x30 
	jb .end
	cmp cl, 0x39
	ja .end
	sub rcx, 0x30 

	mov r11, 10
	push rdx 
	mul r11 
	pop rdx 

	add rax, rcx 
	inc rdx 
	jmp .loop

	.end:
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
   	cmp byte[rdi], '+' 
	je .parse
	cmp byte[rdi], '-' 
	jne parse_uint
	.parse:
	push rdi 
	inc rdi 
	call parse_uint 
	pop rdi 
	inc rdx 
	cmp byte[rdi], '+' 
	je .return
	neg rax 
	.return:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx
    xor rax, rax
    .loop:
    cmp rcx, rdx
    jge .err
    mov al, [rdi+rcx]
    mov  byte [rsi+rcx],  al
    inc rcx
    cmp byte[rdi+rcx],0
    je .end
    jmp .loop
    .err:
    xor rax, rax
    ret
    .end:
    mov byte [rsi+rcx],0
    mov rax, rcx
    ret

.printError:
	push rdi
	call string_length
	mov rdx, rax
	pop rdi
	mov rsi, rdi
	mov rax, 1
	mov rdi, 2
	syscall
	jmp print_newline
	call exit

