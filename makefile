ASM = nasm

.PHONY: clean

%.o: %.asm
	$(ASM) -f elf64 $< 

main.o: main.asm colon.inc lib.inc
	$(ASM) -f elf64 $<


main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o

clean:
	rm *o. main
